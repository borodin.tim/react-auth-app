import { useState } from 'react';

const useHttp = props => {
    const [error, setError] = useState(null);
    const [resData, setResData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccessful, setIsSuccessful] = useState(false);

    const sendRequest = async (url, reqData = null) => {
        setIsLoading(true);
        setError(null);
        let response;
        let data;
        try {
            response = await fetch(url, reqData);
            data = await response.json();

            if (!response.ok) {
                throw new Error('Requiest failed!');
            }

            setResData(data);
            setIsSuccessful(true);
        } catch (error) {
            let errorMsg = error.message || 'Request failed!';
            if (data && data.error && data.error.message) {
                errorMsg = data.error.message;
                if (errorMsg.includes('EMAIL_EXISTS')) {
                    errorMsg = 'Email already registered';
                } else if (errorMsg.includes('INVALID_EMAIL')) {
                    errorMsg = 'Email is invalid';
                } else if (errorMsg.includes('WEAK_PASSWORD')) {
                    errorMsg = 'Password should be at least 6 characters';
                } else if (errorMsg.includes('INVALID_PASSWORD') || errorMsg.includes('EMAIL_NOT_FOUND')) {
                    errorMsg = 'Wrong credentials';
                } else if (errorMsg.includes(' CREDENTIAL_TOO_OLD_LOGIN_AGAIN') || errorMsg.includes(' TOKEN_EXPIRED') || errorMsg.includes(' INVALID_ID_TOKEN')) {
                    errorMsg = 'Credentials invalid. Please login again';
                }
            }
            setError(errorMsg);
            setIsSuccessful(false);
        }
        setIsLoading(false);
    };

    return { resData, error, isLoading, isSuccessful, sendRequest };
};

export default useHttp;