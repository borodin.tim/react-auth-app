import React, { useContext, useEffect, useRef } from 'react';
import { Link, useHistory } from 'react-router-dom';

import classes from './LoginForm.module.css';
import Button from '../UI/Button';
import useHttp from '../../hooks/useHttp';
import authContext from '../../store/auth-context';

const LoginForm = props => {
    const emailRef = useRef();
    const passwordRef = useRef();
    const { resData, error, isLoading, isSuccessful, sendRequest } = useHttp();
    const authCtx = useContext(authContext);
    const history = useHistory();

    useEffect(() => {
        if (error) {
            alert(error);
        }
    }, [error]);

    useEffect(() => {
        if (isSuccessful) {
            const expirationTime = new Date(new Date().getTime() + (+resData.expiresIn * 1000));
            authCtx.login(resData.idToken, expirationTime.toISOString());
            history.replace('/');
        }
    }, [isSuccessful]);

    const handleFormSubmit = async (event) => {
        event.preventDefault();

        const enteredEmail = emailRef.current.value;
        const enteredPassword = passwordRef.current.value;

        // TODO: add validation
        if (enteredEmail.length === 0) {
            return;
        }
        if (enteredPassword.length === 0) {
            return;
        }

        sendRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.REACT_APP_FIREBASE_WEB_API_KEY}`, {
            method: 'POST',
            body: JSON.stringify({
                email: enteredEmail,
                password: enteredPassword,
                returnSecureToken: true
            }),
            headers: { 'Content-Type': 'application/json' }
        });
    }

    return (
        <form className={classes.form} onSubmit={handleFormSubmit}>
            <h2>Login</h2>
            <div className={classes['input-group']}>
                <label htmlFor="email">Email</label>
                <input
                    id="email"
                    type="email"
                    placeholder="Enter email"
                    autoComplete="false"
                    ref={emailRef}
                />
            </div>
            <div className={classes['input-group']}>
                <label htmlFor="password">Password</label>
                <input
                    id="password"
                    type="password"
                    placeholder="Enter password"
                    ref={passwordRef}
                />
            </div>
            {!isLoading && <Button
                className={classes.btn}
                type={'submit'}
            >
                Login
            </Button>}
            {isLoading && <Button
                // eslint-disable-next-line no-sequences
                className={classes['btn-loading', 'btn']}
                type={'submit'}
            >
                Logging in...
            </Button>}
            <p className={classes.link}>
                <Link to='/register'>Create new account</Link>
            </p>
        </form>
    );
};

export default LoginForm;