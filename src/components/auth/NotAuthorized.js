import React from 'react';
import { Link } from 'react-router-dom';

import classes from './NotAuthorized.module.css';

const NotAuthorized = props => {
    return (
        <div className={classes.card}>
            <p className="centered">You are not logged in</p>
            <p className={classes.link}>
                <Link to='/login'>Login into an existing account</Link>
            </p>
        </div>
    );
};

export default NotAuthorized;