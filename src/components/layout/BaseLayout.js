import React from 'react';

import classes from './BaseLayout.module.css';
import Header from './Header';

const BaseLayout = props => {
    return (
        <div>
            <Header />
            <main className={classes.main}>
                {props.children}
            </main>
        </div>
    );
};

export default BaseLayout;