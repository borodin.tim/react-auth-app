import React, { Suspense, useContext } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import BaseLayout from './components/layout/BaseLayout';
import authContext from './store/auth-context';

const Home = React.lazy(() => import('./pages/Home'));
const Login = React.lazy(() => import('./pages/Login'));
const Profile = React.lazy(() => import('./pages/Profile'));
const Register = React.lazy(() => import('./pages/Register'));

function App() {
  const authCtx = useContext(authContext);

  return (
    <BaseLayout>
      <Suspense fallback={'Loading...'}>
        <Switch>
          <Route path='/' exact>
            <Home />
          </Route>
          {!authCtx.isLoggedIn && (
            <Route path='/login'>
              <Login />
            </Route>
          )}
          <Route path='/profile'>
            {authCtx.isLoggedIn && <Profile />}
            {!authCtx.isLoggedIn && <Redirect to='/login' />}
          </Route>
          {!authCtx.isLoggedIn && (
            <Route path="/register">
              <Register />
            </Route>
          )}
          <Route path='*'>
            <Redirect to='/' />
          </Route>
        </Switch>
      </Suspense>
    </BaseLayout>
  );
}

export default App;
