import React from 'react';

import LoginForm from '../components/auth/LoginForm';

const Login = props => {
    return (
        <div>
            <LoginForm />
        </div>
    );
};

export default Login;