import React from 'react';

import ProfileForm from '../components/profile/ProfileForm';

const Profile = props => {
    return (
        <div>
            <ProfileForm />
        </div>
    );
};

export default Profile;