import React from 'react';

import RegisterForm from '../components/auth/RegisterForm';

const Register = props => {
    return (
        <div>
            <RegisterForm />
        </div>
    );
};

export default Register;